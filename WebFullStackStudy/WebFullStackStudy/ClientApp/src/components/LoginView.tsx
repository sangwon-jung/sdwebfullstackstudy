﻿import * as React from 'react';
import Avatar from '@material-ui/core/Avatar';
import Button from '@material-ui/core/Button';
import CssBaseline from '@material-ui/core/CssBaseline';
import TextField from '@material-ui/core/TextField';
import LockOutlinedIcon from '@material-ui/icons/LockOutlined';
import Typography from '@material-ui/core/Typography';
import { makeStyles } from '@material-ui/core/styles';
import Container from '@material-ui/core/Container';
import { RouteComponentProps, withRouter } from 'react-router';

const useStyles = makeStyles((theme) => ({
    paper: {
        marginTop: theme.spacing(8),
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
    },
    avatar: {
        margin: theme.spacing(1),
        backgroundColor: theme.palette.secondary.main,
    },
    form: {
        width: '100%', // Fix IE 11 issue.
        marginTop: theme.spacing(1),
    },
    submit: {
        margin: theme.spacing(3, 0, 2),
    },
}));

const Login = ({ history }: RouteComponentProps) => {
    const classes = useStyles();

    const initialState = {
        email: '',
        password: ''
    }
    const [state, setState] = React.useState(initialState);
    const { email, password } = state;

    const onChange = (e: React.ChangeEvent<HTMLInputElement>) => {
        const { name, value } = e.target;

        setState({
            ...state,
            [name]: value
        })

    }

    // LoginController를 호출합니다
    // POST 방식으로 FormData로 이메일과 비밀전호를 전송합니다
    // 서버 방식에 따라서 자유롭게 변경하셔도 됩니다
    async function requestLogin() {
        const form = new FormData()
        form.append("email", email)
        form.append("password", password)
        const response = await fetch("login", {
            method: "POST",
            body: form
        })
        const data = await response.json()
        return data;
    }

    // 로그인을 호출하는 부분입니다
    // 응답 코드가 1001(OK)인 경우에 welcome 페이지로 전환됩니다
    // 서버 방식에 따라서 자유롭게 변경하셔도 됩니다
    const signIn = async (e: React.FormEvent<HTMLFormElement>) => {
        e.preventDefault();

        const data = await requestLogin();
        console.log(data);

        if (data.code == 1001) {
            history.push("/welcome");
        }
    }

    return (
        <Container component="main" maxWidth="xs">
            <CssBaseline />
            <div className={classes.paper}>
                <Avatar className={classes.avatar}>
                    <LockOutlinedIcon />
                </Avatar>
                <Typography component="h1" variant="h5">
                    Sign in
                </Typography>
                <form className={classes.form} noValidate onSubmit={signIn}>
                    <TextField
                        variant="outlined"
                        margin="normal"
                        required
                        fullWidth
                        id="email"
                        label="Email Address"
                        name="email"
                        autoComplete="email"
                        value={email}
                        autoFocus
                        onChange={onChange}
                    />
                    <TextField
                        variant="outlined"
                        margin="normal"
                        required
                        fullWidth
                        name="password"
                        label="Password"
                        type="password"
                        id="password"
                        value={password}
                        autoComplete="current-password"
                        onChange={onChange}
                    />
                    <Button
                        type="submit"
                        fullWidth
                        variant="contained"
                        color="primary"
                        className={classes.submit}
                    >
                        Sign In
                    </Button>

                </form>
            </div>
        </Container>
    );
}
export default withRouter(Login)
