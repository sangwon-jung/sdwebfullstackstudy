﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebFullStackStudy_Test.Models
{
    public class ApplicationUser : IdentityUser
    {
    }
}
